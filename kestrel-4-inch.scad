// 4-inch Kestrel

// Extreme H
// 8x3 mm or 6x4 mm arms?
// 12 mm motor mounts for 1606 motors

// 16mm == 22XX/23XX/24XX
// 12mm == 13XX/14XX/16XX
//  9mm == 11XX

include<kestrel.scad>

prop_dia=25.4*4;
front_arm_length=75;
front_arm_angle=57;
rear_arm_length=75;
rear_arm_angle=57;

motor_hole=m2_hole;
motor_hole_outer_dia=12;
motor_hole_inner_dia=12;
motor_center_hole=4.8;  //  ~4.8 for 1106, ~6.8 for 22XX?
motor_hole_t=motor_hole_outer_dia*0.55;

arm_width=7;

