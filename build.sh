#! /bin/bash

mkdir -p output/dxf output/svg


for x in side_plates wedge 2mm 1mm; do
  echo Building $x for kestrel...
  openscad -o "output/svg/kestrel-$x.svg" -D 'show_assembly=0' -D "show_$x=1" "falcon-frames.scad" && echo svg Done
  openscad -o "output/dxf/kestrel-$x.dxf" -D 'show_assembly=0' -D "show_$x=1" "falcon-frames.scad" && echo dxf Done
done

for model in kestrel-3-inch kestrel-3-inch-compact kestrel-4-inch; do
  sed -e "s/kestrel-3-inch.scad/$model.scad/" falcon-frames.scad > "$model-tmp-kestrel.scad"

  for x in arms; do
    echo Building $x for $model...
    openscad -o "output/svg/$model-$x.svg" -D 'show_assembly=0' -D "show_$x=1" "$model-tmp-kestrel.scad" && echo svg Done
    openscad -o "output/dxf/$model-$x.dxf" -D 'show_assembly=0' -D "show_$x=1" "$model-tmp-kestrel.scad" && echo dxf Done
  done

  rm "$model-tmp-kestrel.scad"
done

for x in partial_truncated_bottom_plate full_truncated_bottom_plate bottom_plate brace_plate front_truncated_bottom_plate; do
  model="falcon"
  sed -e "s/kestrel-3-inch.scad/falcon-6-inch.scad/" falcon-frames.scad > "$model-tmp-kestrel.scad"

  echo Building $x for $model...
  
  openscad -o "output/svg/$model-$x.svg" -D 'show_assembly=0' -D "show_$x=1" "$model-tmp-kestrel.scad" && echo svg Done
  openscad -o "output/dxf/$model-$x.dxf" -D 'show_assembly=0' -D "show_$x=1" "$model-tmp-kestrel.scad" && echo dxf Done
done

for model in falcon-5-inch falcon-6-inch; do
  sed -e "s/kestrel-3-inch.scad/$model.scad/" falcon-frames.scad > "$model-tmp-kestrel.scad"

  for x in arms; do
    echo Building $x for $model...
    openscad -o "output/svg/$model-$x.svg" -D 'show_assembly=0' -D "show_$x=1" "$model-tmp-kestrel.scad" && echo svg Done
    openscad -o "output/dxf/$model-$x.dxf" -D 'show_assembly=0' -D "show_$x=1" "$model-tmp-kestrel.scad" && echo dxf Done
  done

  rm "$model-tmp-kestrel.scad"
done
