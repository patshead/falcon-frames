// 6-inch Falcon frame

// 16mm == 22XX/23XX/24XX
// 12mm == 13XX/14XX
//  9mm == 11XX

include<falcon.scad>

prop_dia=25.4*6;      // for visualization
front_arm_length=105;
front_arm_angle=45;
rear_arm_length=105;
rear_arm_angle=45;

motor_hole=m3_hole;
motor_hole_outer_dia=16;
motor_hole_inner_dia=16;
motor_center_hole=6.8;  //  ~4.8 for 1106, ~6.8 for 22XX?
motor_hole_t=motor_hole_outer_dia*0.55;

arm_width=12;

// These are for visual layouts only!
falcon_top_offset=falcon_partial_truncated_offset;
falcon_bottom_offset=falcon_full_truncated_offset;

