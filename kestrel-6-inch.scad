// 6-inch Kestrel

//  This is ridiculous.

// 16mm == 22XX/23XX/24XX
// 12mm == 13XX/14XX
//  9mm == 11XX

include<kestrel.scad>

prop_dia=25.4*6;
front_arm_length=90;
front_arm_angle=70;
rear_arm_length=120;
rear_arm_angle=40;

motor_hole=m3_hole;
motor_hole_outer_dia=16;
motor_hole_inner_dia=16;
motor_center_hole=6.8;  //  ~4.8 for 1106, ~6.8 for 22XX?
motor_hole_t=motor_hole_outer_dia*0.55;

arm_width=8;

