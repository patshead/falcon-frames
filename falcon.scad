// Variables shared by all Falcon layouts

arm_mount_spacing=57/2;  // was 49/2
arm_mount_spacing_lateral=57/2;

arm_mount_cutout=0;

wedge_end_width=4;
wedge_width=3;

falcon_arm_mount_spacing=14;

frame_width=27;          // floss/flowride/flosstyle gopro mount width
frame_length=132+10+20-6;  // 22mm for camera, 10mm for rear standoff
frame_forward=13;         // why is this half what I expect?
frame_t=3;

bottom_plate_width=12;     // Was arm_width

falcon=1;

//  0 for three stacks
// 30 for extra battery strap on top plate
// 45 for shortes bottom plate

falcon_partial_truncated_offset=17;
falcon_full_truncated_offset=42;

//falcon_top_offset=0;
//falcon_bottom_offset=0;
