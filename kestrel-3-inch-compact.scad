// 3-inch Kestrel with short, compact arms
// THIS IS UNTESTED.
// Quickly convertion based on original prototype.
// May be a bit off.

// 1106 only
// Very H

// 16mm == 22XX/23XX/24XX
// 12mm == 13XX/14XX
//  9mm == 11XX

prop_dia=25.4*3;      // for visualization
front_arm_length=46;
front_arm_angle=55;
rear_arm_length=46;
rear_arm_angle=55;

motor_hole=m2_hole;
motor_hole_outer_dia=9;
motor_hole_inner_dia=9;
motor_center_hole=4.8;  //  ~4.8 for 1106, ~6.8 for 22XX?
motor_hole_t=motor_hole_outer_dia*0.55;

arm_width=6;

arm_mount_spacing=30.5/2;
arm_mount_spacing_lateral=arm_mount_spacing;

arm_mount_cutout=1;

wedge_end_width=3;
wedge_width=2;

frame_width=20+t*2;
frame_length=124;
frame_forward=0;
frame_t=2;
