// Copyright (C) 2019 Pat Regan - Licensed under the GNU GPL 2

t=4;
hex=4.85; // just over an 8mm hex (radius)
c=0.8;

wrench_t=6;

small_hex=3.2; // m3 nut (radius)

length=38;

offset=-5;

module prop_tool(t, hex, c, wrench_t, small_hex, length, offset) {

projection()
difference() {

  union() {
  translate([offset, 0, 0])
    cylinder(r=hex+wrench_t/2-0.3, h=2);
    hull() {
      translate([-length/2, 0, 0])
      cylinder(d=t, h=2, $fn=18);
      translate([length/2, 0, 0])
      cylinder(d=t, h=2, $fn=18);
      }
    translate([-length/2, 0, 0])
      cylinder(d=wrench_t*2, h=2, $fn=18);
    translate([length/2, 0, 0])
      cylinder(d=5+t/2, h=2, $fn=18);

      
  }

  translate([offset, 0, 0])
  rotate([0, 0, 30])
  union() {
    cylinder(r=hex, h=2, $fn=6);

    for (i=[0, 60, 120, 180, 240, 300]) {
      rotate([0, 0, i])
        translate([hex-c, 0, 0]) cylinder(d=2.1, h=t, $fn=12);
    }
  }

  translate([length/2, 0, 0])
  cylinder(d=5, h=2, $fn=12);

  translate([-length/2, 0, 0])
    union() {
    cylinder(r=small_hex, h=2, $fn=6);

    for (i=[0, 60, 300]) {
      rotate([0, 0, i])
        translate([small_hex-1.1, 0, 0]) cylinder(d=2.1, h=t, $fn=18);
    }
  }

  // The cube to cut the end off the M3 wrench
  // isn't nearly parametric enough.
  // It will be placed incorrectly if the tool
  // changes length.  Maybe I'll fix it someday?!
  
  translate([-(small_hex+10)-length/2-1.5, -10, 0])
    cube([small_hex+10, 20, 2]);
}

}

// Short tool
prop_tool(4, 4.85, 0.8, 6, 3.2, 38, -5);

// Long tool for patshead.com "logo"
//translate([11, -20, 0])
//prop_tool(6, 4.85, 0.8, 6, 3.2, 60, -12);

