// Copyright (C) 2019 Pat Regan - Licensed under the GNU GPL 2
//
// Fillet modules from https://github.com/clothbot/ClothBotCreations/blob/master/utilities/fillet.scad


$fn=24;
t=3;

m3_standoff=6;

enable_props=0;
enable_camera=0;
enable_fov=0;
enable_stack=0;

show_exploded=0;
show_assembly=1;
show_arms=0;
show_side_plates=0;
show_wedge=0;
show_2mm=0;
show_1mm=0;

show_bottom_plate=0;
show_top_plate=0;
show_brace_plate=0;

show_partial_truncated_bottom_plate=0;
show_full_truncated_bottom_plate=0;
show_front_truncated_bottom_plate=0;  // For DJI FPV Air Module

show_falcon_test=0;
show_falcon_dji=0;

m2_hole=2.6;
m3_hole=3.42; // was 3.50 and loose
m3_locknut=6;

center_stack_hole_size=m3_hole; // grommet will convert to m2

//include<falcon-5-inch.scad>;
//include<falcon-6-inch.scad>;
include<kestrel-3-inch.scad>;
//include<kestrel-3-inch-compact.scad>;
//include<kestrel-4-inch.scad>;
//include<kestrel-6-inch.scad>;

motor_hole_outer_spacing=sqrt(motor_hole_outer_dia * motor_hole_outer_dia / 2);
motor_hole_inner_spacing=sqrt(motor_hole_inner_dia * motor_hole_inner_dia / 2);
motor_mount_dia=sqrt(2 * motor_hole_outer_spacing * motor_hole_outer_spacing);

arm_tolerance=0.005;
arm_wedge_tolerance=0.025;

top_plate_t=2;
top_plate_length=38;
top_plate_width=20;
battery_strap_width=15+2;  //only used in Kestrel?

tab_length=20+3+3;
tab_width=5.2;       // 4.9
tab_depth=5;
grommet_width=9.5;   // 9
grommet_height=6.4;  // 5.9
grommet_tab_distance=124;

stack_height=22;  // was 20
side_t=3;
side_cam_t=2;
front_grommet_offset=-17.5;

cam_radius=15;    // was 12.5
cam_forward=3;    // was 10
cam_up=17;        // was 14


module fillet(r=1.0,steps=3,include=true) {
  if(include) for (k=[0:$children-1]) {
      children(k);
    }
  for (i=[0:$children-2] ) {
    for(j=[i+1:$children-1] ) {
      fillet_two(r=r,steps=steps) {
        children(i);
        children(j);
        intersection() {
          children(i);
          children(j);
        }
      }
    }
  }
}

module fillet_two(r=1.0,steps=3) {
  for(step=[1:steps]) {
    hull() {
      render() intersection() {
        children(0);
        offset_3d(r=r*step/steps) children(2);
      }
      render() intersection() {
        children(1);
        offset_3d(r=r*(steps-step+1)/steps) children(2);
      }
    }
  }
}

module offset_3d(r=1.0) {
  for(k=[0:$children-1]) minkowski() {
      children(k);
      sphere(r=r,$fn=8);
    }
}

module battery_5s_1300() {
  translate([-90/2, -35/2, 0])
  cube([90, 35, 35]);
}

module dji_air_module() {
  translate([-22, -19, 0])
  cube([44, 38, 15]);
}

module gopro_session() {
  translate([-38/2, -38/2, 0])
  cube([38, 38, 38]);
}

module fake_camera(s) {
  translate([-s/2, s/2, 7])
    cube([s, s, s]);

  if(enable_fov) {
    translate([0, s*1.6, 7+s/2])
      rotate([-90, 0, 0])
      scale([1,0.3,1])
      #cylinder(d1=1, d2=172*2, h=43);
      }
}

module fake_prop(dia) {
  difference() {
    color("gray") cylinder(d=dia, h=t*3);
    translate([0, 0, -0.1]) color("gray") cylinder(d=dia-t, h=t*3+1);    
  }

  //color("pink") cylinder(d=13, h=t*3);
  
}

module stack_holes(s, d) {
  translate([-s/2, -s/2, 0]) cylinder(d=d, h=15);
  translate([s/2, -s/2, 0]) cylinder(d=d, h=15);
  translate([-s/2, s/2, 0]) cylinder(d=d, h=15);
  translate([s/2, s/2, 0]) cylinder(d=d, h=15);
}

module motor_holes(t) {
  translate([0, 0, -.25])
    rotate([0, 0, 0])
    union() {
    hull() {
      translate([-motor_hole_outer_spacing/2, -motor_hole_outer_spacing/2])
        cylinder(d=motor_hole, h=t+0.5); // *1.1 to help alignment?
      translate([-motor_hole_inner_spacing/2, -motor_hole_inner_spacing/2])
        cylinder(d=motor_hole, h=t+0.5); // *1.1 to help alignment?
    }
    hull() {
      translate([motor_hole_outer_spacing/2, -motor_hole_outer_spacing/2])
        cylinder(d=motor_hole, h=t+0.5);
      translate([motor_hole_inner_spacing/2, -motor_hole_inner_spacing/2])
        cylinder(d=motor_hole, h=t+0.5);
    }
    hull() {
      translate([-motor_hole_outer_spacing/2, motor_hole_outer_spacing/2])
        cylinder(d=motor_hole, h=t+0.5);
      translate([-motor_hole_inner_spacing/2, motor_hole_inner_spacing/2])
        cylinder(d=motor_hole, h=t+0.5);
    }
    hull() {
      translate([motor_hole_outer_spacing/2, motor_hole_outer_spacing/2])
        cylinder(d=motor_hole, h=t+0.5);
      translate([motor_hole_inner_spacing/2, motor_hole_inner_spacing/2])
        cylinder(d=motor_hole, h=t+0.5);
    }
  }
}

module motor_mount_back(t) {
  hull() {
    translate([-motor_hole_outer_spacing/2, -motor_hole_outer_spacing/2])
      cylinder(d=motor_hole+motor_hole_t, h=t);
    translate([-motor_hole_outer_spacing/2, motor_hole_outer_spacing/2])        
      cylinder(d=motor_hole+motor_hole_t, h=t);
  }
}

module motor_mount(t) {
  difference() {
    fillet(r=motor_hole_outer_spacing/3, steps=12)
      //union()
    {
      hull() {
        translate([motor_hole_outer_spacing/2, motor_hole_outer_spacing/2])        
          cylinder(d=motor_hole+motor_hole_t, h=t);
        rotate([0, 0, 40])
          translate([motor_mount_dia*1.2, 0, 0])
          cylinder(d=motor_mount_dia/3, h=t);
      }
      hull() {
        translate([motor_hole_outer_spacing/2, -motor_hole_outer_spacing/2])
          cylinder(d=motor_hole+motor_hole_t, h=t);
        rotate([0, 0, -40])
          translate([motor_mount_dia*1.2, 0, 0])
          cylinder(d=motor_mount_dia/3, h=t);
      }
      motor_mount_back(t);
      cylinder(d=motor_hole_outer_dia, h=t);
    }
    
    motor_holes(t);
  }

  if(enable_props) {
    translate([0, 0, 4])
      fake_prop(prop_dia);
  }
  
}

module falcon_three_stacks(offset) {
  if(offset==0) {
    translate([0, 20+18+3.5+6, 0]) color("red") stack_holes(20, m2_hole);
  }
  translate([0, 0, 0]) color("green") stack_holes(20, m3_hole);
  translate([0, 0, 0]) color("green") stack_holes(30.5, m3_hole);
  translate([0, -20-18-3.5-6, 0]) color("blue") stack_holes(20, m2_hole);
}

module falcon_three_stacks_with_holes(offset) {
  slot_length=20;
  slot_width=4;

  big_cut=16;
  slot_offset=14/2; // was 16/2
  
  if(offset==0) {
    translate([-14/2, 20+18+3.5-slot_length/2+6, 0]) cube([slot_width, slot_length, 20]);
  } else if (offset==falcon_partial_truncated_offset) {
    translate([-14/2, 20+18+3.5-slot_length/2+6-6, 0]) cube([slot_width, slot_length, 20]);
  }
  translate([-14/2, -slot_length/2, 0]) cube([slot_width, slot_length, 20]);
  translate([-14/2, -20-18-3.5-slot_length/2-6, 0]) cube([slot_width, slot_length, 20]);

  mirror([1, 0, 0]){
  if(offset==0) {
    translate([-14/2, 20+18+3.5-slot_length/2+6, 0]) cube([slot_width, slot_length, 20]);
  } else if (offset==falcon_partial_truncated_offset) {
    translate([-14/2, 20+18+3.5-slot_length/2+6-6, 0]) cube([slot_width, slot_length, 20]);
  }
  translate([-14/2, -slot_length/2, 0]) cube([slot_width, slot_length, 20]);
  translate([-14/2, -20-18-3.5-slot_length/2-6, 0]) cube([slot_width, slot_length, 20]);
  }

  // front cutout
  translate([-slot_length/2, -69-6, 0])
    cube([slot_length, slot_width*0.8, 20]);

  // toilet tank rear
  translate([-slot_length/2, 16, 0])
  cube([slot_length, slot_width*1.2, 20]);

  // toilet tank front
  mirror([0, 1, 0])
  translate([-slot_length/2, 16, 0])
    cube([slot_length, slot_width*1.2, 20]);

  /*
  translate([-big_cut/2, -39, 0])
  cube([big_cut, big_cut/2, 20]);

  if(offset==0) {
    translate([-big_cut/2, 23+big_cut/2, 0])
      cube([big_cut, big_cut/2, 20]);
    }
    */
}


module kestrel_three_stacks() {
  translate([0, 20+18+3.5, 0]) color("red") stack_holes(20, m2_hole);
  translate([0, 0, 0]) color("green") stack_holes(20, center_stack_hole_size);
  translate([0, -20-18-3.5, 0]) color("blue") stack_holes(20, m2_hole);
}

module kestrel_three_stacks_with_holes() {
  translate([-17/2, 20+18+3.5-15/2, 0]) cube([6.5, 15, 20]);
  translate([-17/2, -15/2, 0]) cube([6.5, 15, 20]);
  translate([-17/2, -20-18-3.5-15/2, 0]) cube([6.5, 15, 20]);

  mirror([1, 0, 0]){
  translate([-17/2, 20+18+3.5-15/2, 0]) cube([6.5, 15, 20]);
  translate([-17/2, -15/2, 0]) cube([6.5, 15, 20]);
  translate([-17/2, -20-18-3.5-15/2, 0]) cube([6.5, 15, 20]);
  }
}

module kestrel_bottom_plate_extra_holes() {
  translate([-17/2+4, 20+18-15/2+20+2, 0]) cube([2.5, 8, 20]);
  //translate([-17/2, 20+18-15/2-18, 0]) cube([6.5, 12+3, 20]);
  //translate([-17/2, 20+18-3-15/2-56, 0]) cube([6.5, 12+3, 20]);
  translate([-17/2+4, -20-18-15/2-15, 0]) cube([2.5, 8, 20]);

  mirror([1, 0, 0]){
  translate([-17/2+4, 20+18-15/2+20+2, 0]) cube([2.5, 8, 20]);
  //translate([-17/2, 20+18-15/2-18, 0]) cube([6.5, 12+3, 20]);
  //translate([-17/2, 20+18-3-15/2-56, 0]) cube([6.5, 12+3, 20]);
  translate([-17/2+4, -20-18-15/2-15, 0]) cube([2.5, 8, 20]);
  }
}


module dummy() {
  if(enable_camera) {
    translate([0, 54-8,16]) color("orange") fake_camera(19);
  }

  if(enable_stack) {
    kestrel_three_stacks();
  }
} 

module arm_mount_holes(s, t) {
  translate([0, 0, -0.5])
    cylinder(d=s, h=t+1);

//  translate([0, -m3_hole*2.7, -0.5])  // this is bad news
//    cylinder(d=s, h=t+1);
}

module arm_mount_not_holes(s, t) {
  translate([0, 0, 0])
    cylinder(d=s, h=t);

  translate([0, -m3_hole*.7, 0])  // this is bad news
    cylinder(d=s, h=t);

}

module arm_hole_diff(t) {
      translate([0, falcon_arm_mount_spacing, 0])
        cylinder(d=m3_hole, h=t+1);
      translate([0, -falcon_arm_mount_spacing, 0])
        cylinder(d=m3_hole, h=t+1);
}

module arm_wedge_diff(t) {
  translate([0, 0, -0.5])
  union() {
    translate([-1, 1, 0])
      cylinder(d=wedge_end_width+(arm_wedge_tolerance*2), h=t+1);
    hull() {
      translate([0, 5, 0])
        cylinder(d=wedge_end_width+(arm_wedge_tolerance*2), h=t+1);
      translate([0, -5, 0])
        cylinder(d=wedge_end_width+(arm_wedge_tolerance*2), h=t+1);
    }
    hull() {
      translate([0, 0, 0])
        cylinder(d=wedge_width+(arm_wedge_tolerance*2), h=t+1);
      translate([-8, 0, 0])
        cylinder(d=wedge_width+(arm_wedge_tolerance*2), h=t+1);
    }
  }
}

module arm(angle, length, t) {  // Arm centered on outside mounting screw hole
  x=sin(angle)*length;
  y=cos(angle)*length;

  short_x=sin(angle)*(length-motor_hole_outer_spacing*0.7);
  short_y=cos(angle)*(length-motor_hole_outer_spacing*0.7);

  translate([-m3_hole*3/2, 0, 0])  // not parametric enough!
    difference() {
    union() {
      fillet(r=4, steps=6) {
        hull() {
          translate([0, arm_tolerance, 0]) cube([m3_hole*3, 1, t]);
          translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            cylinder(d=m3_hole*3, h=t);
        }

        hull() {
          translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            cylinder(d=arm_width, h=t);

          translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            translate([short_y, short_x, 0])
            cylinder(d=arm_width, h=t);
        }  
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
          translate([y, x, 0])
          rotate([0, 0, angle])
          motor_mount_back(t);
      }
      translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        translate([y, x, 0])
        rotate([0, 0, angle])
        motor_mount(t);

      if(falcon) {
        hull() {
          translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            cylinder(d=arm_width, h=t);
          
          translate([m3_hole*1.5-8, arm_mount_spacing_lateral-8, 0])
            cylinder(d=arm_width*0.5, h=t);

          translate([0, arm_tolerance, 0]) cube([m3_hole*3, 1, t]);
        }
      }
      
    }

    translate([m3_hole*3/2, arm_mount_spacing_lateral, 0])  // not parametric enough!
      arm_mount_holes(m3_hole, t);

    translate([m3_hole*3/2, 0, 0])  // Centered on center of arm wedge bone
      arm_wedge_diff(t);
    
    translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
      translate([y, x, 0])
      rotate([0, 0, angle])
    {
      motor_holes(t);
      translate([0, 0, -0.5])
        cylinder(d=motor_center_hole, h=t+2);
    }

    if (falcon) {
      translate([m3_hole*3/2, falcon_arm_mount_spacing, 0])
        cylinder(d=m3_hole, h=t+1);
    } else {
      translate([0, 10, -1])
        cylinder(r=3.5, h=t+2);
    }
//    translate([10, 10, -1])
//    cylinder(r=3, h=t+2);
    
    /* translate([m3_hole*3/2-2, 10.25, -1]) */
    /*   resize([m3_hole*1.6, m3_hole*1.25, t+2]) */
    /*   cylinder(d=m3_hole, h=t+2); */
  }
}

module tab_base(t) {
  translate([-tab_width/2, -tab_length/2, 0])
    cube([tab_width, tab_length, t]);
}

module tab_cutout(t) {
  translate([-(grommet_width+7)/2, -tab_length/2, -0.5])
    cube([grommet_width+7, tab_depth, t+1]);

  translate([-(grommet_width+7)/2, -tab_length/2 + tab_length-tab_depth, -0.5])
    cube([grommet_width+7, tab_depth, t+1]);

}

module arm_wedge() {
  hull() {
    translate([0, -5, 0])
      cylinder(d=wedge_end_width, h=t);
    translate([0, 5, 0])
      cylinder(d=wedge_end_width, h=t);
  }

  hull() {
    translate([0, 0, 0])
      cylinder(d=wedge_width, h=t);
    translate([-arm_mount_spacing*2, 0, 0])
      cylinder(d=wedge_width, h=t);
  }

  translate([-arm_mount_spacing*2, 0, 0])
  hull() {
    translate([0, -5, 0])
      cylinder(d=wedge_end_width, h=t);
    translate([0, 5, 0])
      cylinder(d=wedge_end_width, h=t);
  }
  
}

module falcon_arm_brace(t) {
  extra=2;
  difference() {
    union() {  // was fillet(r=2)
      union() {
        translate([-17, -17, 0])
        cube([34, 34, t]);
        translate([-0.7, 0, 0])
        hull() {  //front
          translate([-bottom_plate_width*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
            translate([bottom_plate_width*1.5+1, arm_mount_spacing_lateral, 0])
          hull() arm_mount_not_holes(bottom_plate_width*0.5+extra, t);
          mirror([0, 1, 0])
            translate([-bottom_plate_width*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
            translate([bottom_plate_width*1.5+1, arm_mount_spacing_lateral, 0])
            hull() arm_mount_not_holes(bottom_plate_width*0.5+extra, t);
        }
        translate([0.7, 0, 0])
        hull() {  //rear
          mirror([1, 0, 0])
            translate([-bottom_plate_width*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
            translate([bottom_plate_width*1.5+1, arm_mount_spacing_lateral, 0])
            hull() arm_mount_not_holes(bottom_plate_width*0.5+extra, t);
          
          mirror([0, 1, 0])
            mirror([1, 0, 0])
            translate([-bottom_plate_width*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
            translate([bottom_plate_width*1.5+1, arm_mount_spacing_lateral, 0])
            hull() arm_mount_not_holes(bottom_plate_width*0.5+extra, t);
        }
      }
/*
      hull() {  // side
        translate([-bottom_plate_width*3/2-arm_mount_spacing, 0.6, 0]) // not parametric enough!
          translate([bottom_plate_width*1.5, arm_mount_spacing_lateral, 0])
          cylinder(d=bottom_plate_width, h=t);

        mirror([1, 0, 0])
          translate([-bottom_plate_width*3/2-arm_mount_spacing, 0.6, 0]) // not parametric enough!
          translate([bottom_plate_width*1.5, arm_mount_spacing_lateral, 0])
          cylinder(d=bottom_plate_width, h=t);

      }

      hull() {  // side
        mirror([0, 1, 0])
          translate([-bottom_plate_width*3/2-arm_mount_spacing, 0.6, 0]) // not parametric enough!
          translate([bottom_plate_width*1.5, arm_mount_spacing_lateral, 0])
          cylinder(d=bottom_plate_width, h=t);
      
        mirror([0, 1, 0])
          mirror([1, 0, 0])
          translate([-bottom_plate_width*3/2-arm_mount_spacing, 0.6, 0]) // not parametric enough!
          translate([bottom_plate_width*1.5, arm_mount_spacing_lateral, 0])
          cylinder(d=bottom_plate_width, h=t);
      }
*/
      hull() {  // half X
        translate([-arm_mount_spacing_lateral-extra, arm_mount_spacing_lateral+extra, 0]) // not parametric enough!
          cylinder(d=bottom_plate_width, h=t);

        mirror([0, 1, 0])
          mirror([1, 0, 0])
          translate([-arm_mount_spacing_lateral-extra, arm_mount_spacing_lateral+extra, 0]) // not parametric enough!
          cylinder(d=bottom_plate_width, h=t);

      }

      hull() {  // half X
        mirror([0, 1, 0])
          translate([-arm_mount_spacing_lateral-extra, arm_mount_spacing_lateral+extra, 0]) // not parametric enough!
          cylinder(d=bottom_plate_width, h=t);
      
        mirror([1, 0, 0])
          translate([-arm_mount_spacing_lateral-extra, arm_mount_spacing_lateral+extra, 0]) // not parametric enough!
          cylinder(d=bottom_plate_width, h=t);
      }

    }
    union() {
      translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);
      
      mirror([0, 1, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);
    
      mirror([1, 0, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);

      mirror([0, 1, 0])
        mirror([1, 0, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);
    }

    translate([0, 0, -1]) {
      stack_holes(20, m3_hole);
      stack_holes(30.5, m3_hole);
    }
    translate([arm_mount_spacing, 0, -0.5])
      arm_hole_diff(t+2); 
    translate([-arm_mount_spacing, 0, -0.5])
      arm_hole_diff(t+2); 

  }

}


module falcon_bottom_plate(t, offset) {
  camera_cutout=12+m3_standoff;
  camera_side_width=9;
  
  union() {
    difference() {
      union() {
        fillet(r=4, steps=6) {
          translate([-frame_length/2 + frame_forward, -frame_width/2, 0])
            union() {
            hull() {
              translate([-m3_standoff/2+offset+1.5, -m3_hole/2, 0])
                cylinder(d=m3_standoff, h=t);
              translate([frame_length+m3_standoff/2-camera_cutout, -m3_hole/2, 0])
                cylinder(d=m3_standoff, h=t);
              translate([-m3_standoff/2+offset+1.5, frame_width+m3_hole/2, 0])
                cylinder(d=m3_standoff, h=t);
              translate([frame_length+m3_standoff/2-camera_cutout, frame_width+m3_hole/2, 0])
                cylinder(d=m3_standoff, h=t);
            }
            union() {
              hull() {
                translate([frame_length+m3_hole/2-camera_cutout, -m3_hole+(camera_side_width/2-m3_hole/2), 0])
                  cylinder(d=camera_side_width, h=t);
                translate([frame_length+m3_hole/2-1.5, -m3_hole+(camera_side_width/2-m3_hole/2), 0])
                  cylinder(d=camera_side_width, h=t);
              }
              hull() {
                translate([frame_length+m3_hole/2-camera_cutout, m3_hole+frame_width-(camera_side_width/2-m3_hole/2), 0])
                  cylinder(d=camera_side_width, h=t);
                translate([frame_length+m3_hole/2-1.5, m3_hole+frame_width-(camera_side_width/2-m3_hole/2), 0])
                  cylinder(d=camera_side_width, h=t);
              }
            }
          }

          hull() {
            translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
              translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
              cylinder(d=bottom_plate_width, h=t);

            mirror([0, 1, 0])
              translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
              translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
              cylinder(d=bottom_plate_width, h=t);
          }

          hull() {
            mirror([1, 0, 0])
              translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
              translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
              cylinder(d=bottom_plate_width, h=t);

            mirror([0, 1, 0])
              mirror([1, 0, 0])
              translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
              translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
              cylinder(d=bottom_plate_width, h=t);  // Why this math?
          }
        }
        falcon_arm_brace(t);
     }
      
      translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);

      mirror([0, 1, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t); 
    
      mirror([1, 0, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);

      mirror([0, 1, 0])
        mirror([1, 0, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);

      rotate([0, 0, 90])
        translate([0, 0, -5])
        falcon_three_stacks(offset);

      rotate([0, 0, 90])
        translate([0, 0, -5])
        falcon_three_stacks_with_holes(offset);

      translate([-frame_length/2 + frame_forward, -frame_width/2, 0])
        union() {
        translate([0, 0, -1])
          cylinder(d=m3_hole, h=t+2);
        translate([frame_length, 0, -1])
          cylinder(d=m3_hole, h=t+2);
        translate([0, frame_width, -1])
          cylinder(d=m3_hole, h=t+2);
        translate([frame_length, frame_width, -1])
          cylinder(d=m3_hole, h=t+2);
        translate([frame_length-25, frame_width/2, -1]) // 25 mm for TPU GoPro hole
          cylinder(d=m3_hole, h=t+2);

      }

      translate([arm_mount_spacing, 0, -0.5])
        arm_hole_diff(t); 
      translate([-arm_mount_spacing, 0, -0.5])
        arm_hole_diff(t); 
    }
  }
}

module kestrel_bottom_plate(t) {
  union() {
    difference() {
      fillet(r=4, steps=6) {
        translate([-frame_length/2 + frame_forward, -frame_width/2, 0])
          hull() {
          translate([t/2, t/2, 0])
            cylinder(d=t, h=t);
          translate([frame_length-t/2, t/2, 0])
            cylinder(d=t, h=t);
          translate([t/2, frame_width-t/2, 0])
            cylinder(d=t, h=t);
          translate([frame_length-t/2, frame_width-t/2, 0])
            cylinder(d=t, h=t);
        }

        hull() {
          translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
            translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            cylinder(d=m3_hole*3*1.3, h=t);

          mirror([0, 1, 0])
            translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
            translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            cylinder(d=m3_hole*3*1.3, h=t);
    
        }

        hull() {
          mirror([1, 0, 0])
            translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
            translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            cylinder(d=m3_hole*3*1.3, h=t);

          mirror([0, 1, 0])
            mirror([1, 0, 0])
            translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
            translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            cylinder(d=m3_hole*3*1.3, h=t);
    
        }
      }

      translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);

      mirror([0, 1, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);
    
      mirror([1, 0, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);

      mirror([0, 1, 0])
        mirror([1, 0, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0])  // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);

      rotate([0, 0, 90])
        translate([0, 0, -5])
        kestrel_three_stacks();

      rotate([0, 0, 90])
        translate([0, 0, -5])
        kestrel_three_stacks_with_holes();

      rotate([0, 0, 90])
        translate([0, 0, -5])
        kestrel_bottom_plate_extra_holes();
      
      translate([-62-0.5, 0, 0])
        tab_cutout(t);

      translate([-62+grommet_tab_distance+0.5, 0, 0])
        tab_cutout(t);
    
    }

    translate([-62, 0, 0])
      tab_base(t);

      translate([-62+grommet_tab_distance, 0, 0])
        tab_base(t);
      
  }

}

module kestrel_arm_brace() {
  difference() {
    union() {
      union() {
        translate([-0.7, 0, 0])
        hull() {
          translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
            translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
          hull() arm_mount_not_holes(m3_hole*1.6, 1);
          mirror([0, 1, 0])
            translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
            translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            hull() arm_mount_not_holes(m3_hole*1.6, 1);
        }
        translate([0.7, 0, 0])
        hull() {
          mirror([1, 0, 0])
            translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
            translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            hull() arm_mount_not_holes(m3_hole*1.6, 1);
          
          mirror([0, 1, 0])
            mirror([1, 0, 0])
            translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
            translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
            hull() arm_mount_not_holes(m3_hole*1.6, 1);
        }
      }

      hull() {
        translate([-m3_hole*3/2-arm_mount_spacing, 0.6, 0]) // not parametric enough!
          translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
          cylinder(d=m3_hole*1.2, h=1);

        mirror([1, 0, 0])
          translate([-m3_hole*3/2-arm_mount_spacing, 0.6, 0]) // not parametric enough!
          translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
          cylinder(d=m3_hole*1.2, h=1);

      }

      hull() {
        mirror([0, 1, 0])
          translate([-m3_hole*3/2-arm_mount_spacing, 0.6, 0]) // not parametric enough!
          translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
          cylinder(d=m3_hole*1.2, h=1);
      
        mirror([0, 1, 0])
          mirror([1, 0, 0])
          translate([-m3_hole*3/2-arm_mount_spacing, 0.6, 0]) // not parametric enough!
          translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
          cylinder(d=m3_hole*1.2, h=1);
      }

      hull() {
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
          translate([m3_hole*1.5, frame_width/5, 0])
          cylinder(d=m3_hole*1.2, h=1);

        mirror([0, 1, 0])
          mirror([1, 0, 0])
          translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
          translate([m3_hole*1.5, frame_width/5, 0])
          cylinder(d=m3_hole*1.2, h=1);

      }

      hull() {
        mirror([0, 1, 0])
          translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
          translate([m3_hole*1.5, frame_width/5, 0])
          cylinder(d=m3_hole*1.2, h=1);
      
        mirror([1, 0, 0])
          translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
          translate([m3_hole*1.5, frame_width/5, 0])
          cylinder(d=m3_hole*1.2, h=1);
      }

    }
    union() {
      translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);
      
      mirror([0, 1, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);
    
      mirror([1, 0, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);

      mirror([0, 1, 0])
        mirror([1, 0, 0])
        translate([-m3_hole*3/2-arm_mount_spacing, 0, 0]) // not parametric enough!
        translate([m3_hole*1.5, arm_mount_spacing_lateral, 0])
        arm_mount_holes(m3_hole, t);
    }
  
  }

}


module grommet_hole(w, h, d) {
  resize([w, h, d])
    cylinder(d=2, h=d);
}


module side_cam() {
  difference() {
    union() {
      translate([cam_forward, cam_up, 0])
        rotate([-90, 0, 0])
        union() {
        difference() {
          rotate([90, 0, 0])
            cylinder(r=cam_radius, h=t);

          rotate([0, 40, 0])
            translate([-50, -t-1, -100])
            cube([100, t*2, 100]);

          rotate([0, -115, 0])
            translate([-50, -t-1, 0])
            cube([100, t*2, 100]);

          rotate([90, 0, 0])
            translate([0, 0, -1])
            cylinder(r=cam_radius-side_cam_t, h=t+3);
        }

        rotate([90, 0, 0])
          cylinder(d=m2_hole*2.2, h=t);

//        rotate([0, 0, 0])
//          translate([0, -t, -t*.6/2])
//          cube([cam_radius-4, 2.4, t*.6]);

        rotate([0, 0, 0])
          translate([0, -t, -t*.6/2+m2_hole/2])
          cube([cam_radius-2, 2.4, t*.6]);

        /* rotate([0, 160, 0]) */
        /*   translate([0, -t, -t/2]) */
        /*   cube([cam_radius, 2, t*.6]); */

        fillet(r=2, steps=4) {
          hull() {
            rotate([90, 0, 0])
              translate([2, 0, 0])
              cylinder(d=side_cam_t, h=t);

            rotate([90, 0, 0])
              translate([-cam_forward-5, -cam_up, 0])
              cylinder(d=side_cam_t, h=t);

            }

          rotate([90, 0, 0])
          translate([-cam_forward, -cam_up, 0])
          grommet_hole(grommet_width+4, grommet_height+4, t);
          }
        
        fillet(r=2, steps=4) {
          hull() {
            rotate([90, 0, 0])
              translate([0, 2, 0])
              cylinder(d=side_cam_t, h=t);

            rotate([90, 0, 0])
              translate([front_grommet_offset-cam_forward, stack_height + (t/2) + (side_t/2) -cam_up-2, 0])
              cylinder(d=side_cam_t, h=t);
          }
          rotate([90, 0, 0])
            translate([front_grommet_offset-cam_forward, stack_height + (t/2) + (side_t/2) -cam_up, 0])
            cylinder(d=m3_hole*2, h=t);

        }

        fillet(r=2, steps=4) {
          hull() {
            rotate([0, 40, 0])
              translate([cam_radius-side_t/2+0.5, 0, 0])
              rotate([90, 0, 0])
              cylinder(d=side_cam_t, h=t);

            rotate([90, 0, 0])
              translate([-cam_forward, -cam_up-3, 0])
              cylinder(d=side_cam_t, h=t);
          }
          rotate([90, 0, 0])
          translate([-cam_forward, -cam_up, 0])
          grommet_hole(grommet_width+4, grommet_height+4, t);

        }
        
        fillet(r=2, steps=4) {
          hull() {
            rotate([0, -115, 0])
              translate([cam_radius-side_t/2+0.5, 0, 0])
              rotate([90, 0, 0])
              cylinder(d=side_cam_t, h=t);
            
            rotate([90, 0, 0])
              translate([front_grommet_offset-cam_forward, stack_height + (t/2) + (side_t/2) -cam_up+2, 0])
              cylinder(d=side_cam_t, h=t);
          }
            rotate([90, 0, 0])
              translate([front_grommet_offset-cam_forward, stack_height + (t/2) + (side_t/2) -cam_up, 0])
              cylinder(d=m3_hole*2, h=t);

        }

// Third Standoff        
//        rotate([90, 0, 0])
//          translate([-1, -14, 0])
//          cylinder(d=m3_hole*2, h=t);
        
      }
  
      grommet_hole(grommet_width+4, grommet_height+4, t);
  
  
    }
    translate([cam_forward, cam_up, -0.5])
      cylinder(d=m2_hole, h=t+1);

// Third Standoff    
//      translate([9, 0, -0.5])
//      cylinder(d=m3_hole, h=t+1);    
  }


}

module side_plate() {
  bl=battery_strap_width+t*3;
  translate([-60, 0, 0])
    rotate([90, 0, 0])
    difference() {
    union() {
      fillet (r=2, steps=2) {
        grommet_hole(grommet_width+4, grommet_height+4, t);
        hull() {
          cylinder(d=side_t, h=t);
          translate([3, stack_height + (t/2) + (side_t/2), 0])
            cylinder(d=side_t, h=t);
        }
        translate([3, stack_height + (t/2) + (side_t/2), 0])
          cylinder(d=m3_hole*2, h=t);

        difference() {
          union() {
          hull() {
            translate([3, stack_height + (t/2) + (side_t/2), 0])
              cylinder(d=side_t, h=t);

            translate([grommet_tab_distance+front_grommet_offset, stack_height + (t/2) + (side_t/2), 0])
              cylinder(d=side_t, h=t);
          }
          translate([48-top_plate_t/2, stack_height + (t/2) + (side_t/2)+1, 0])
            hull() {
            cylinder(d=top_plate_t*3, h=t);
            translate([bl+0.4+top_plate_t, 0, 0])
              cylinder(d=top_plate_t*3, h=t);
          }

          }
          translate([48, stack_height + (t/2) + (side_t/2)+1, -0.5])
            hull() {
            cylinder(d=2.25, h=3+1);
            translate([bl+0.4, 0, 0])
              cylinder(d=2.25, h=3+1);
          }
 
        }
        translate([grommet_tab_distance+front_grommet_offset, stack_height + (t/2) + (side_t/2), 0])
          cylinder(d=m3_hole*2, h=t);

        /* hull() { */
        /*   translate([grommet_tab_distance+front_grommet_offset, stack_height + (t/2) + (side_t/2), 0]) */
        /*     cylinder(d=2, h=t); */
        /*   translate([grommet_tab_distance, 0, 0]) */
        /*     cylinder(d=2, h=t); */

        /* } */
        
        translate([grommet_tab_distance, 0, 0]) grommet_hole(grommet_width+3, grommet_height+3, t);

      }
      translate([grommet_tab_distance, 0, 0])
        side_cam();
    }
    translate([0, 0, -0.5]) grommet_hole(grommet_width, grommet_height, t+1);

    translate([grommet_tab_distance, 0, -.5]) grommet_hole(grommet_width, grommet_height, t+1);

    translate([3+0.5, stack_height + (t/2) + (side_t/2) -0.5, -0.5])
      cylinder(d=m3_hole, h=t+1);

    translate([grommet_tab_distance+front_grommet_offset, stack_height + (t/2) + (side_t/2), -0.5])
      cylinder(d=m3_hole, h=t+1);
    
    
  }
}

module kestrel_top_plate(t) {
  bl=battery_strap_width+t*4;
  bw=3+5;
  
  translate([-top_plate_length/2, -top_plate_width/2, 0])
    union() {
    difference() {
      cube([top_plate_length, top_plate_width, t]);

      for(i=[0:5]) {
        translate([t + (t*i + t*2*i), t, -0.1])
          cube([t*2, top_plate_width-t*2, t+1]);
      }
    }

    translate([top_plate_length/2, top_plate_width+bw+0, 0])
      translate([-bl/2, -bw-0, 0])
      difference() {
      cube([bl, bw+0.5, t]);
      translate([t*2, bw+1-(2.3/2)-t-1, -0.5])
        hull() {
        cylinder(d=3.6, h=t+1);
        translate([bl-t*4, 0, 0])
          cylinder(d=3.6, h=t+1);
      }
    }

    translate([top_plate_length/2, 0, 0])
      translate([-bl/2, -bw, 0])
      difference() {
      translate([0, -0.5, 0])
      cube([bl, bw+0.5, t]);
    
      translate([t*2, 3, -0.5])
        hull() {
        cylinder(d=3.6, h=t+1);
        translate([bl-t*4, 0, 0])
          cylinder(d=3.6, h=t+1);
      }
    }
  }
}

module falcon_front_bumper(t) {
  camera_cutout=12+m3_standoff;
  camera_side_width=9;

  difference() {
    hull() {
      translate([0, -m3_hole+(camera_side_width/2-m3_hole/2), 0])
        cylinder(d=camera_side_width, h=t);
      translate([0, m3_hole+frame_width-(camera_side_width/2-m3_hole/2), 0])
        cylinder(d=camera_side_width, h=t);
    }
    union() {
      translate([0, -m3_hole+(camera_side_width/2-m3_hole/2), -1])
        cylinder(d=m3_hole, h=t+2);
      translate([0, m3_hole+frame_width-(camera_side_width/2-m3_hole/2), -1])
        cylinder(d=m3_hole, h=t+2);
    }
    }
}

module render_exploded() {
  mirror([0, 0, 0])
    translate([arm_mount_spacing, 0, -3])
    arm(front_arm_angle, front_arm_length); // motor 4

  mirror([0, 1, 0])
    translate([arm_mount_spacing, 0, -3])
    arm(front_arm_angle, front_arm_length); // motor 2

  mirror([1, 0, 0])
    translate([arm_mount_spacing, 0, -3])
    arm(rear_arm_angle, rear_arm_length);  // motor 3

  mirror([1, 0, 0])
    mirror([0, 1, 0])
    translate([arm_mount_spacing, 0, -3])
    arm(rear_arm_angle, rear_arm_length); // motor 1

  translate([arm_mount_spacing, 0, -3])
    arm_wedge();
  
  rotate([0, 0, -90])
    dummy();

  translate([0, 0, 15])
    kestrel_bottom_plate(2);

  translate([-2, -30, 16])  // -2 because round grommet bits aren't centerer right?
    side_plate();
  translate([-2, 30, 16])
    side_plate();

  translate([0, 0, -20])
      kestrel_arm_brace();

  translate([-1, 0, 15+22])
    kestrel_top_plate(top_plate_t);
}


module render_assembled() {
  mirror([0, 0, 0])
    translate([arm_mount_spacing, 0, -3])
    arm(front_arm_angle, front_arm_length, 4); // motor 4

  mirror([0, 1, 0])
    translate([arm_mount_spacing, 0, -3])
    arm(front_arm_angle, front_arm_length, 4); // motor 2

  mirror([1, 0, 0])
    translate([arm_mount_spacing, 0, -3])
    arm(rear_arm_angle, rear_arm_length, 4);  // motor 3

  mirror([1, 0, 0])
    mirror([0, 1, 0])
    translate([arm_mount_spacing, 0, -3])
    arm(rear_arm_angle, rear_arm_length, 4); // motor 1

  translate([arm_mount_spacing, 0, -3]) color("purple")
    arm_wedge(4);

  translate([0, 0, -16])
  rotate([0, 0, -90])
    dummy();

  if(falcon) {
    if (show_falcon_dji) {
      translate([0, 0, 1]) color("blue")
        difference() {
        falcon_bottom_plate(2, 0);
        translate([34.5, -44/2, -1])
        cube([200, 44, 4]);
        }
    
      translate([0, 0, -5]) color("green")
        falcon_bottom_plate(2, falcon_bottom_offset);
      translate([57, 0, -3]) color("orange") dji_air_module();
    } else {
      translate([0, 0, -5]) color("blue")
        falcon_arm_brace(2);
    
      translate([0, 0, 1]) color("green")
        falcon_bottom_plate(2, falcon_bottom_offset);
    }

    translate([0, 0, 25]) color("cyan")
        falcon_bottom_plate(2, falcon_top_offset);
    
//    translate([frame_length/2+frame_forward+2, -13.5, -3]) color("cyan")
//      falcon_front_bumper(t);

    translate([-5, 0, 30]) color("grey")
      battery_5s_1300();

    translate([80, 0, 44]) color("grey") rotate([0, -30, 0])
      gopro_session();

  } else {
    translate([0, 0, 0]) color("green")
      kestrel_bottom_plate(2);

    translate([0, 0, -4]) color("blue")
      kestrel_arm_brace(1);
    translate([-2, -10, 1])  // -2 because round grommet bits aren't centerer right?
      color("orange")
      side_plate(3);

    translate([-2, 13, 1])
      color("orange")
      side_plate(3);

    translate([-1, 0, stack_height+4.2-0.15]) color("cyan")
      kestrel_top_plate(top_plate_t);
  }

}

module render_arms() {
    rotate([0, 0, -front_arm_angle+90]) arm(front_arm_angle, front_arm_length, t);
//    translate([4, -22, 0])
//    rotate([0, 0, +front_arm_angle+90]) mirror([0, 1, 0]) arm(front_arm_angle, front_arm_length, t);

    translate([40, 0, 0])
    rotate([0, 0, -rear_arm_angle+90]) arm(rear_arm_angle, rear_arm_length,t);
//    translate([44, -22, 0])
//    rotate([0, 0, +rear_arm_angle+90]) mirror([0, 1, 0]) arm(rear_arm_angle, rear_arm_length, t);
}

module render_wedge() {
  translate([-10, 122, 0])
  rotate(90, 0, 0)
    arm_wedge();
  /*
  translate([8, 122, 0])
  rotate(90, 0, 0)
    arm_wedge();
  translate([8+18, 122, 0])
  rotate(90, 0, 0)
    arm_wedge();
  translate([8+36, 122, 0])
  rotate(90, 0, 0)
    arm_wedge();
    */
}

module render_side_plates() {
  translate([-10, 0, 0])
      rotate([90, 0, 90]) side_plate();

  translate([0, 27, 0])
    mirror([1, 0, 0])
    rotate([90, 0, 90]) side_plate();
}

module render_2mm() {
  kestrel_bottom_plate(2);

  translate([grommet_tab_distance-top_plate_length+4, 0, 0]) kestrel_top_plate(top_plate_t);
}


module render_bottom_plate(offset, front) {
  if(falcon) {
    if(front) {  // this feels like cheating!
      difference() {
        falcon_bottom_plate(2, offset);
        translate([34.5, -44/2, -1])
        cube([200, 44, 3]);
      }
    } else {
      falcon_bottom_plate(2, offset);
    }
  } else {
    kestrel_bottom_plate(2);
  }
}


module render_top_plate() {
  if(falcon) {
    falcon_bottom_plate(2, falcon_top_offset);
  } else {
    kestrel_top_plate(2);
  }
}

module render_brace_plate() {
  if(falcon) {
    falcon_arm_brace(2);
  } else {
    kestrel_arm_brace(1);
  }
}

module render_1mm() {
  kestrel_arm_brace();
}

module render_falcon_test() {
  difference() {
    union() {
      intersection() {
        difference() {
          arm(front_arm_angle, front_arm_length, t);
        }
        translate([-15, -10, 0])
          cube([30, 30, 20]);
      }
      translate([-6, 8, 0])
        cube([12, 12, t]);
    }
    translate([0, 11, -0.5])cylinder(d=m2_hole, h=t+1);
    translate([0, 15, -0.5])cylinder(d=m3_hole, h=t+1);
  }

  translate([0, -12, 0])
    intersection() {
    arm_wedge();
    translate([-10, -15, 0])
      cube([30, 30, 20]);
  }
    
}

if (show_assembly) { render_assembled(); }
if (show_exploded) { render_exploded(); }
if (show_arms) { projection() render_arms(); }
if (show_side_plates) { projection() translate([89, 38, 0]) render_side_plates(); }
if (show_wedge) { projection() render_wedge(); }
if (show_2mm) { projection() render_2mm(); }
if (show_1mm) { projection() render_1mm(); }

if (show_bottom_plate) { projection() render_bottom_plate(0); }
if (show_partial_truncated_bottom_plate) { projection() render_bottom_plate(falcon_partial_truncated_offset); }
if (show_full_truncated_bottom_plate) { projection() render_bottom_plate(falcon_full_truncated_offset); }
if (show_front_truncated_bottom_plate) { projection() render_bottom_plate(0, 1); }
if (show_top_plate) { projection() render_top_plate(); }
if (show_brace_plate) { projection() render_brace_plate(); }
if (show_falcon_test) { projection() render_falcon_test(); }
