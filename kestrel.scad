// Variables shared by all Kestrel layouts

arm_mount_spacing=30.5/2;
arm_mount_spacing_lateral=arm_mount_spacing;

arm_mount_cutout=1;

wedge_end_width=3;
wedge_width=2;

frame_width=20+t*2;
frame_length=124;
frame_forward=0;
frame_t=2;

falcon=0;
