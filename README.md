# falcon-frames

The Falcon frames are two open-source FPV miniquad frame designs.  The Kestrel is a lightweight HD micro FPV freestyle frame with an Acrobrat-style suspension system sized for sub-250 gram builds with 2.5", 3", or 4" props.  It is named after the American Kestrel--a small, pigeon-sized falcon.

The Kestrel is probably production ready.  I've been flying the ones I cut in my garage for months.

The Falcon is a sturdy, heavy 5" or 6" FPV freestyle miniquad frame.  It is named to keep with the theme of the Kestrel.  I've cut one Falcon so far.  It is a 6" frame, and it flies exactly like I expected it to.  It has some minor bugs, but nothing that prevents me from flying!

## WARNING: Things are a bit out of sync!

I've been doing a bad job keeping things up to date.  I've been making slow and steady improvements to the 3"/4" Kestrel, and I'm been making crazy experimental changes to the 5"/6" Falcon.  

I don't like uploading changes unless I've cut and tested the parts, and I have lots of Falcon changes in the OpenSCAD that aren't ready to be flown.  The trouble is that there are also significant improvements to the Kestrel in that same source code.  What to do?!

I committed up-to-date Kestrel SVG and DXF files.  These can't currently be generated from the OpenSCAD source in the repository.  I'm going to work on correcting this situation soon.

Here's what's been changed due to Kestrel testing in the real world:

 * Top plate sits about 2mm higher
 * Side plates are about 0.5mm wider--so much stiffer now!
 * "Knuckle" for the top-rear standoff is much bigger
 * Camera mount is beefier
 * The "dog bone" is shorter and narrower
 * The base of the arms are strengthened in 3 places
 * Arms are a bit more tapered and slightly narrower
 
The first three changes will keep your flight controller safe if you decide to fly heavier batteries than I intended.

My test pilot has broken the mounting points, and we made the weak points stronger each time.

I haven't gotten to cut all the parts separately to weigh them as a set.  The arms are stronger, but are nearly identical in weight to the previous revision.  I'm certain I weighed the side plates, but I don't remember the exact difference.  The Kestrel might be a gram or two heavier now.

The 3" SVG and DXF files should match my test pilot's current build.  I haven't had to upgrade my own 4" Kestrel to the stronger arms yet.

I've been saying the new arms are indestructible.  I know that is an exaggeration, but as long as you keep under 250 grams, I don't think the Kestrel can break its arms under its own power.  Of course they will eventually break.  Every time you crash you stress the fibers and the epoxy, and the parts get a bit weaker.  I'm betting fresh arms will stand up to 250 gram crashes with no problem, and they should last a long time.  I hope!

## Changes are coming to the Kestrel

First, I'm going to eliminate the 3" compact model.  I just don't like it.

The standard 3" Kestrel arms are just about as long as I'm comfortable making them.  Their length is based on the motor wires of an Emax 1306b motor and a Diatone Mamba Mini ESC.  They're about a millimeter away from being able to support 4" props (I've squeezed a set of t-mount 4x2x2 props on before!), but I just can't make them any longer.

I'm planning to split the difference between the old compact and standard 3" models, and I'm going to just call it the 3.5" Kestrel.  I think 3.5" props are one of the best options for a sub-250 build.

The 4" Kestrel is nearly big enough for 5" props.  It is quite huge!  There's room to swing a 5" prop between the fuselage and motor, but the front and rear motors aren't spaced far enough apart.  Now that there are some really gentle 5" biblade props, I'm more than a little tempted to make room for them.

I'm either going to push the 4" up to support those gentle 5" props, or I'm going to squish things together a bit.  I haven't decided yet.  Maybe I'll do both!

## The Kestrel HD Micro Frame

The Kestrel is an open-source HD micro quadcopter frame.  It was designed using OpenSCAD, and I attempted to make the frame as parametric as possible.

![My 3" Kestrel](https://blog.patshead.com/Assets/Kestrel6.jpg) 

The parametric parts of the design include:

 * Motor mount hole spacing
 * Arm length
 * Arm angle
 * Arm width
 * FPV camera's protection diameter
 * FPV camera's relative positioning
 * Side plate thickness
 * Height available to the stack
 * Grommet hole length and height
 * Grommet tab depth and width

The default configuration of the frame is for a 3" build.  The arms and side plates are cut from a 3 mm thick sheet of carbon.  The top and bottom plates are 2 mm thick.  The underside bracing plate is 1 mm.  

![My 3" Kestrel](https://blog.patshead.com/Assets/Kestrel1.png) 


Aside from carbon fiber, the frame requires the following hardware:

 * (2x) 20 mm aluminum standoffs
 * (4x) 5 mm M3 screws
 * (4x) 10 mm M3 screws
 * (4x) M3 lock nuts
 * (4x) [Rubber grommets][g] for side plate
 * (4x) [Rubber grommets][g2] for M2 center stack

You can [read more about the Kestrel frame][b] on my blog.

<blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="en" dir="ltr">I&#39;m quite happy with how much nicer the ND16 filter makes to the Caddx Turtle&#39;s video. It still can&#39;t compete with a GoPro, but it is quite good such an inexpensive piece of hardware. Especially one that also replaces my $40 FPV camera! <a href="https://t.co/9xMs9MvzD8">pic.twitter.com/9xMs9MvzD8</a></p>&mdash; Pat Regan (@patsheadcom) <a href="https://twitter.com/patsheadcom/status/1120075384450617344?ref_src=twsrc%5Etfw">April 21, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### What's new in version 1.1?

Everything is still compatible with version 1.0.  

The long 3" arms are 2 mm shorter.  I had some motor wires that were a stretch, so I wanted to make sure that wouldn't be problematic.  I also removed some of the weight-saving cutouts near the center of the bottom plate to save weight.

I finally broke a Kestrel.  It was one of my 4" Kestrel's arms.  It broke at the inside end of the arm between the mounting screw and the "wedge" that holds the arms in place.  I didn't think there would be all that much force in that spot, so I cut out some material that I thought was unnecessary.

It is necessary!  I didn't realize that on collisions at the just the right angle, the arm would try to pivot around the screw.  When it does, it puts a lot of stress on that point.  It is almost twice as wide at that point now, so I don't think this will happen again!

## The Falcon Freestyle Miniquad Frame

This is still in the testing phase.  I don't even have a list of nuts and bolts ready yet.  I didn't even have enough of the correct sizes on hand when I assembled the first prototype, so I'm kind of improvising!

I've been flying the Hyperlite Flowride for almost a year now.  The Flowride, FlosStyle, and Glide have all be huge influences in my planning so far.

Here are the important design choices that drove the current layout:

 * Top and bottom plates are interchangeable
   * This drives the location of the battery strap slots
   * The battery straps fit through the slots in each stack
 * Up to three stacks
   * Center stack has holes for 20x20mm or 30.5x30.5mm
   * Front stack is 20x20mm
   * Rear stack is 20x20mm and optional
 * Three top/bottom plate options
   * Full-length with all three stacks
   * Partially truncated plate with 2 stacks and two battery straps
   * Fully truncated plate with 2 stacks and one battery strap
   
I'm currently flying a 6" Falcon with 4mm thick, 12mm wide arms in a true-X configuration with a fully truncated bottom plate and a partially truncated top plate.

Photos, screenshots, and videos will be coming soon.

 * [Pat talking about the Falcon][fy1] on YouTube
 * [Maiden Flight of the Falcon][fy2] on YouTube


[b]: https://blog.patshead.com/2019/03/the-kestrel-hd-micro-quad-frame-version-1-dot-0.html "The Kestrel HD Micro Quad Frame Version 1.0"
[g]: https://www.mcmaster.com/9307K12 "Grommets at McMaster-Carr"
[g2]: https://www.mcmaster.com/9600K73 "Grommets at McMaster-Carr"
[fy1]: https://www.youtube.com/watch?v=6BEEZA434eE "Talking About the First Falcon FPV Freestyle Frame Prototype"
[fy2]: https://www.youtube.com/watch?v=tJ9TNaOIuhc "Maiden Flight of the Falcon: My New Open Source FPV Freestyle Frame"
