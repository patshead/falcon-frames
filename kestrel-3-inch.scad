// 3-inch Kestrel with long arms
// This is the 3-inch Kestrel that Pat flies

// Not quite true-X.  Just a little H.

// 16mm == 22XX/23XX/24XX
// 12mm == 13XX/14XX
//  9mm == 11XX

include<kestrel.scad>

prop_dia=25.4*3;      // for visualization
front_arm_length=60;
front_arm_angle=50;
rear_arm_length=60;
rear_arm_angle=50;

motor_hole=m2_hole;
motor_hole_outer_dia=12;
motor_hole_inner_dia=9;
motor_center_hole=4.8;  //  ~4.8 for 1106, ~6.8 for 22XX?
motor_hole_t=motor_hole_outer_dia*0.55;

arm_width=6;

